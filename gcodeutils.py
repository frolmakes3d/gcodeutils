#!/usr/bin/python3

import click
import re
import sys
from typing import Optional
import fluentpy as _

_input = sys.stdin
_output = sys.stdout


@click.group()
@click.option('--input')
@click.option('--output')
def actions(input, output):
    if (input != None):
        global _input
        _input = open(input, 'r')
    if (output != None):
        global _output
        _output = open(output, 'w')


@actions.command()
@click.option('--offset')
@click.option('--keep')
def translateZ(offset, keep):
    for line in _input:
        _output.write(doTranslateLine(line,'Z', float(offset), None if keep == None else float(keep)))

@actions.command()
@click.option('--offset')
def translateY(offset   ):
    for line in _input:
        _output.write(doTranslateLine(line,'Y', float(offset), None))

@actions.command()
@click.option('--offset')
def translateX(offset   ):
    for line in _input:
        _output.write(doTranslateLine(line,'X', float(offset), None))

def doTranslateLine(line,axis, offset: float, keep: Optional[float] = None):

    p = re.compile(axis + '([+-]?[0-9]+(\.[0-9]*)?)')
    match = p.search(line)
    if (match != None):
        matchAmount = float(match.group()[1:])
        if (keep == None or matchAmount != keep):
            return line[0:match.start()] + axis + str(float(match.group()[1:]) + offset) + line[match.end():]
        else:
            return line
    else:
        return line


if __name__ == '__main__':
    _(sys.argv).map(lambda x : print('arg:' + x))
    actions()


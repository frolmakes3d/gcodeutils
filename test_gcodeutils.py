import gcodeutils
import unittest

class TestStringMethods(unittest.TestCase):
    def test_translate_z(self):
        self.assertEquals(gcodeutils.doTranslateLineZ("G21 X12 Y3 Z4 F12", 3), "G21 X12 Y3 Z7.0 F12")
        self.assertEquals(gcodeutils.doTranslateLineZ("G21 X12 Y3 Z4.3 F12", 3), "G21 X12 Y3 Z7.3 F12")
        self.assertEquals(gcodeutils.doTranslateLineZ("G21 X12 Y3 F12", 3), "G21 X12 Y3 F12")
        self.assertEquals(gcodeutils.doTranslateLineZ("G21 X12 Y3 Z3 F12", 4,3), "G21 X12 Y3 Z3 F12")
